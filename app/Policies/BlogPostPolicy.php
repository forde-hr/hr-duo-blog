<?php

namespace App\Policies;

use App\Models\BlogPost;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Response;

class BlogPostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, BlogPost $blogPost)
    {
        return $user->id === $blogPost->user_id;
    }
}
