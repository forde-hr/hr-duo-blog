<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class BlogPostController extends Controller
{
    public function index()
    {
       	$posts = BlogPost::all();
		return $posts;
    }

    public function store(Request $request){
        $articleData = $request->request->get('blog');
        $articleData['user_id'] = Auth::user()->id;
            try {
                $article = BlogPost::create(
                    $articleData
                );
                return response()->json([
                    'success' => true,
                    'article' => $article,
                    'message' => "Post has been published correctly"
                ], Response::HTTP_CREATED);
            } catch (\Exception $e) {
                return  response()->json([
                    'message' => $e->getMessage(),
                    'success' => false,
                ], $e->getCode());
            }
    }

    public function update(Request $request){
        $articleData = $request->request->get('blog');
        $articleData['user_id'] = Auth::user()->id;

            try {
                $article = BlogPost::find($articleData['id']);
                if ($request->user()->cannot('update', $article)) {
                    return response()->json([
                        'success' => false,
                        'message' => "You don't have permissions to update this post",
                    ], Response::HTTP_UNAUTHORIZED);
                } else {
                    $article->update(
                        $articleData
                    );
                    return response()->json([
                        'success' => true,
                        'article' => $article,
                        'message' => "Post has been updated correctly"
                    ]);
                }
            } catch (\Exception $e) {
                return  response()->json([
                    'message' => $e->getMessage(),
                    'success' => false,
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
    }

    public function show($id)
	{
        $blogPost = BlogPost::find($id);

        return $blogPost;
    }

    public function showByAuthor($id)
	{
        return User::find($id)->blogPosts;
    }

    public function myBlogs()
	{
        $blogs = Auth::user()->blogPosts;
        return response()->json([
            'success' => true,
            'blogs' => $blogs
        ]);
    }
}
