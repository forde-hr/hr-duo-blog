<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailNewsletter extends Mailable
{
    use Queueable, SerializesModels;
    public $mailData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->mailData = $mailData;
        $this->markdown('email.newsletter');
        $this->subject('Newsletter '.date('F Y'));
        $this->with($this->mailData);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this;

    }
}
