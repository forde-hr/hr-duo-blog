<?php

namespace App\Observers;

use App\Mail\EmailBlogPost;
use App\Models\BlogPost;
use Illuminate\Support\Facades\Mail;

class BlogPostObserver
{
    /**
     * Handle the BlogPost "created" event.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return void
     */
    public function created(BlogPost $blogPost)
    {
        $this->sendMailable($blogPost);
    }

    private function sendMailable(BlogPost $blogPost)
    {
        Mail::to(env('MAIL_TO_ADDRESS'))->send(new EmailBlogPost($blogPost));
    }
}
