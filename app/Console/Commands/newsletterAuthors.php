<?php

namespace App\Console\Commands;

use App\Mail\EmailNewsletter;
use App\Models\BlogPost;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class newsletterAuthors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:newsletter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Newsletter to all blog';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("Cron Newsletter is working fine!");
        $users = User::all();
        foreach ($users as $user) {
            Mail::to($user->email)->send(new EmailNewsletter(['user' => $user, 'posts' => $user->blogPosts]));
        }
    }
}
