import axios from "../interceptor/axiosApi";
const token = sessionStorage.getItem('token');
const initialState = { loggedIn: token ? true : false , user : null} ;

export const auth = {
    namespaced: true,
    state: initialState,
    actions: {
        login({ commit }, user) {
            axios
                .post( '/api/login', user)
                .then(response => {
                    commit('loginSuccess', response.data);
                });
        },
        logout(contex) {
            axios
                .post('/api/logout')
                .then(response => {
                    contex.commit('logout');
                });
        },
        user({ commit }) {
            axios
                .get('/api/me')
                .then(response => {
                    commit('initialiceUser', response.data.user);
                })
                .catch(response => {
                    contex.dispatch('alert/error', response.data, { root: true });
            });;
        },
    },
    mutations: {
        loginSuccess(state, payload) {
            state.loggedIn = true;
            state.user = payload.user;
            if (payload.accessToken) {
                sessionStorage.setItem('token', payload.accessToken);
            }
        },
        initialiceUser(state, payload) {
            state.loggedIn = true;
            state.user = payload;

        },
        loginFailure(state) {
            state.loggedIn = false;
            state.user = null;
            sessionStorage.removeItem('token')
        },
        logout(state) {
            state.loggedIn = false;
            state.user = null;
        }
    },
    getters : {

    }
};
