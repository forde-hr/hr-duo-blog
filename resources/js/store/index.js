import { createStore } from "vuex";
import { auth } from "./auth";
import { articles } from "./articles";
import {alert} from "./alert";

const store = createStore({
    modules: {
        auth,
        articles,
        alert,
    },
});

export default store;
