import axios from "../interceptor/axiosApi";
const state = {
    articles: [],
};
const getters = {};
const actions = {
    articles(context) {
        axios
            .get('/api/myblogs')
            .then((response) => {
                context.commit('initialiceArticles', response.data.blogs)
            })
        ;
    },
    addArticle(context, payload) {
        axios
            .post('/api/blog', {blog: payload})
            .then((response) => {
                context.dispatch('alert/success', 'New post added  successful', { root: true });
                context.commit('addArticle', response.data.article)
            });
    },
    editArticle(context, payload) {
        axios
            .put('/api/blog', { blog: payload})
            .then((response) => {
                context.dispatch('alert/success', 'Post edited successful', { root: true });
                context.commit('replaceArticle', response.data.article)
            });
    },
};
const mutations = {
    initialiceArticles(state, payload) {
        state.articles = payload
    },
    addArticle(state, payload) {
        state.articles.push(payload)
    },
    replaceArticle(state, payload) {
        let pos = state.articles.findIndex(obj => obj.id === payload.id);
        // if its just a replace of an existing article
        if (pos !== -1) {
            state.articles.splice(pos, 1, payload);
        } else {
            console.error('No Matching position!');
        }
    },
};
export const articles = {
    namespaced: true,
    state: state,
    getters,
    actions,
    mutations
};
