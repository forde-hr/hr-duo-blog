
import axiosInstance from "./axiosApi";
const setup = (store) => {
    axiosInstance.interceptors.request.use(
        (config) => {
            let token = sessionStorage.getItem('token')
            if (token) {
                config.headers['Authorization'] = `Bearer ${ token }`
            }
            return config;
        },
        (error) => {
            return Promise.reject(error);
        }
    );
    axiosInstance.interceptors.response.use(
        response => {
            store.dispatch('alert/clear');
            return response

        },
        error => {
            if (error.response.status == 401) {
                if ( typeof error.response.data.message != "undefined" &&  ['Token is Invalid', 'Token is Expired', 'Authorization Token not found'].includes(error.response.data.message)) {
                    store.commit('auth/loginFailure')
                } else {
                    store.dispatch('alert/error', error.response.data.message, { root: true });
                }
            }
            if (error.response.status == 500) {
                store.dispatch('alert/error', error.response.data.message, { root: true });
            }
            return Promise.reject(error)
        }
    )

};

export default setup;
