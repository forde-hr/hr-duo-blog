import { createApp } from 'vue'
import store from "./store";
import App from './App.vue'
import 'vue-universal-modal/dist/index.css'
import VueUniversalModal from 'vue-universal-modal'
export const eventBus = createApp(App)
eventBus.use(VueUniversalModal, {
    teleportTarget: '#modals',
    teleportName: "Modal"
})
eventBus.use(store)
import setup from './interceptor/setup';
setup(store);
eventBus.mount('#app')
