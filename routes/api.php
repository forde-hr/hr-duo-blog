<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\ApiAuthController;
use App\Http\Controllers\BlogPostController;

Route::post('login', [\App\Http\Controllers\ApiAuthController::class, 'login']);
Route::post('logout', [\App\Http\Controllers\ApiAuthController::class, 'logout']);
Route::get('me', [\App\Http\Controllers\ApiAuthController::class, 'getUser'])->middleware(['jwt.verify']);


Route::middleware(['jwt.verify'])->group(function () {
    Route::get('blogs', [BlogPostController::class, 'index']);
    Route::post('blog', [BlogPostController::class, 'store']);
    Route::put('blog', [BlogPostController::class, 'update']);
    Route::get('blog/author/{id}', [BlogPostController::class, 'showByAuthor']);
    Route::get('blog/{id}', [BlogPostController::class, 'show']);
    Route::get('myblogs', [BlogPostController::class, 'myBlogs']);

});
