<p align="center">HR Duo Blog</p>

<p align="center">
	<a href="https://www.hrduo.com"></a></a>
</p>

## About
For this task we ask you to create a simple web application that produces a number of key features;
	-- Create a modal login component which redirects the user to their authenticated blog dashboard.
	-- Display the users blog count history in a component on the dashboard
	-- Allow that user to create/edit their posts
	-- Send email notification to 'aron.cassidy@hrduo.com' when a new post is created.
	-- Update the unauthenticated blog page whenever a change or new post has been added.
	-- Create a cron job to deliver a newsletter to all blog authors once a month on the 5th at 10:00am 		   (Newsletter text irrelevant)
	-- Generate Unit & Feature tests


## Requirements:

	-- Use JWT Auth for session token creation
	-- Use vue store to handle the session management on the client side
	-- Add middleware route protection to authenticated routes
	-- Post Article with input sanitisation [Title => string, max 60, Body => string, max character 250]
	-- Use axios to handle the request on the client side
	-- Notify 'aron.cassidy@hrduo.com' through mail of new post creation -> Sufficent to show email sending through test


## Implementation

To get the project up and running enter the following commands

Laravel: 	composer install
Vue: 		npm install