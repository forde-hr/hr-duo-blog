<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use \App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthTest extends TestCase
{
    use DatabaseMigrations;

    //Create user and authenticate the user
    protected function authenticate($user = null)
    {
        $userForAuth = $user != null ? $user : User::factory()->create();
        return JWTAuth::fromUser($userForAuth);
    }
    /**
     * A login
     *
     * @return void
     */

    /** @test  */
    public function test_login()
    {
        $data = [
            'email' => 'yanet.ennis@hrduo.com',
            'password' => 'hr-duo',
        ];
        $response = $this->post('/api/login', $data);
        $response->assertStatus(401);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->hasAll('status', 'message')
        );

       $user = User::factory(1)->create();
        $data = [
            'email' => $user->toArray()[0]['email'],
            'password' => 'hr-duo',
        ];
        $response = $this->post('/api/login', $data);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
        $json->hasAll('status', 'accessToken', 'user')
            ->missingAll('message', 'code')
        );
    }

    /**
     * A login
     *
     * @return void
     */

    /** @test  */
    public function test_logout()
    {
        $data = [
            'email' => 'yanet.ennis@hrduo.com',
            'password' => 'hr-duo',
        ];
        $response = $this->post('/api/login', $data);
        $response->assertStatus(401);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->hasAll('status', 'message')
        );

       $user = User::factory()->create();
        $data = [
            'email' => $user->toArray()['email'],
            'password' => 'hr-duo',
        ];
        $response = $this->post('/api/login', $data);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
        $json->hasAll('status', 'accessToken', 'user')
            ->missingAll('message', 'code')
        );
    }


    /** @test  */
    public function test_user()
    {
        $user = User::factory()->create();
        $token = $this->authenticate($user);

        $response = $this->withToken($token)->get('/api/me');

        $response
            ->assertJson(fn (AssertableJson $json) =>
            $json
                ->has('user')
                ->has('user', fn ($json) =>
                $json
                    ->where('id', $user->id)
                    ->where('name', $user->name)
                    ->missing('password')
                    ->etc()
                )
            );
    }


    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test  */
    public function a_guest_user_create_post()
    {
        $response = $this->post('/api/blog', ['blog' => ['title' => 'new post', 'body' => 'new post']]);
        $response->assertStatus(401);
    }

    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test  */
    public function a_guest_user_create_dashboard()
    {
        $response = $this->get('/api/myblogs');
        $response->assertStatus(401);
    }
}
