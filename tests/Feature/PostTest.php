<?php

namespace Tests\Feature;

use App\Models\BlogPost;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailBlogPost;
use Mockery as m;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Mail\Markdown;

class PostTest extends TestCase
{
    use DatabaseMigrations;


    //Create user and authenticate the user
    protected function authenticate($user = null)
    {
        $userForAuth = $user ? $user : User::factory()->create();
        return JWTAuth::fromUser($userForAuth);
    }

    /**
     * Create Blog
     *
     * @return void
     */
    /** @test */
    public function a_user_create_post()
    {
        $token = $this->authenticate();
        $response = $this->withToken($token)->post('/api/blog', [
            'blog' => [
                'title' => 'New Post',
                'body' => 'Live as if you were to die tomorrow. Learn as if you were to live forever'
            ]
        ]);
        $response->assertJson(fn(AssertableJson $json) => $json
            ->has('success')
            ->has('message')
            ->has('article', fn($json) => $json
                ->where('id', 1)
                ->where('title', 'New Post')
                ->where('body', 'Live as if you were to die tomorrow. Learn as if you were to live forever')
                ->etc()
            )
        );
        $response->assertStatus(201);
    }

    /**
     * My blogs
     *
     * @return void
     */
    /** @test */
    public function test_api_my_posts()
    {
        $cantPosts = 10;
        $user = User::factory()->create();
        $token = $this->authenticate($user);
        $this->addPosts($user, $cantPosts);
        $response = $this->withToken($token)->get('/api/myblogs');
        $response->assertJson(fn(AssertableJson $json) => $json
            ->has('success')
            ->has('blogs', $cantPosts)
        );
        $response->assertStatus(200);
    }

    /**
     * Update post from other user
     *
     * @return void
     */
    /** @test */
    public function test_api_update_post_from_other_author()
    {
        $cantPosts = 1;
        //create user and assign posts
        $user = User::factory()->create();
        $blogs = $this->addPosts($user, $cantPosts);
        $blog = $blogs[0]->toArray();
        $blog['title'] = $blog['title']. ' edit';
        //authenticated with diferent user and login with.
        $token = $this->authenticate();
        $response = $this->withToken($token)->put('/api/blog', ['blog' => $blog]);
        $response
            ->assertStatus(401)
            ->assertJson([
                'success' => false,
                'message' => "You don't have permissions to update this post",
            ]);
    }

    /**
     * Update post
     *
     * @return void
     */
    /** @test */
    public function test_api_update_post()
    {
        $cantPosts = 1;
        //create user and assign posts
        $user = User::factory()->create();
        $blogs = $this->addPosts($user, $cantPosts);
        $blog = $blogs[0]->toArray();
        $blog['title'] = $blog['title']. ' edit';
        //authenticated with diferent user and login with.
        $token = $this->authenticate($user);
        $response = $this->withToken($token)->putJson('/api/blog', ['blog' => $blog]);
        $response
            ->assertStatus(401)
            ->assertJson([
                'success' => false,
                'message' => "You don't have permissions to update this post",
            ]);
    }

    private function addPosts($user, $cantPosts)
    {
        return BlogPost::factory($cantPosts)->create(['user_id' => $user->id]);
    }
}
