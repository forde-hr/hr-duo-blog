<?php

namespace Tests\Unit;


use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tymon\JWTAuth\Facades\JWTAuth;

class BlogPostTest extends TestCase
{
    use DatabaseMigrations;

    //Create user and authenticate the user
    protected function authenticate($user = null)
    {
        $userForAuth = $user != null ? $user : User::factory()->create();
        return JWTAuth::fromUser($userForAuth);
    }
    /**
     * Create Blog check database
     *
     * @return void
     */
    /** @test */
    public function create_post_check_database()
    {
        $user = User::factory()->create();

        $token = $this->authenticate($user);
        $response = $this->withToken( $token)->post('/api/blog', [
            'blog' => [
                'title' => 'New Post',
                'body' => 'Live as if you were to die tomorrow. Learn as if you were to live forever'
            ]
        ]);
        $data = $response->json();

        $this->assertDatabaseHas('blog_post', [
            'user_id' => $user->id,
            'title' => $data['article']['title'],
            'body' => $data['article']['body'],
        ]);
    }

    /**
     * Test a console command.
     *
     * @return void
     */
    public function test_console_command()
    {
        $this->artisan('send:newsletter')->assertSuccessful();
    }

    /** @test */
    public function it_should_return_an_error_message_on_a_bad_forgot_token(){
        return $this->withToken('token_Invalid')->get('/api/myblogs')
            ->assertJson([
                'message' => 'Token is Invalid'
            ])
            ->assertStatus(401);

    }

    /** @test */
    public function it_should_return_an_error_message_without_token(){
        return $this->get('/api/myblogs')
            ->assertJson([
                'message' => 'Authorization Token not found'
            ])
            ->assertStatus(401);

    }
}
